<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



Route::group([
    'prefix' => 'v1',
    'namespace' => 'Api\v1',
    'middleware' => ['auth:api']
], function(){

    Route::apiResources([
        'posts' => 'PostController',
        'users' => 'UserController',
        'comments' => 'CommentController'
    ]);
    // Route::apiResource('posts','PostController');
    Route::get('/post/{post}/relationship/author', 'PostRelationshipController@autor')->name('posts.relationship.author');
    Route::get('/post/{post}/author', 'PostRelationshipController@autor')->name('posts.author');

    Route::get('/post/{post}/relationship/comments', 'PostRelationshipController@comments')->name('posts.relationship.comments');
    Route::get('/post/{post}/comments', 'PostRelationshipController@comments')->name('posts.comments');


});


Route::post('login', 'AuthController@login');
Route::post('singup', 'AuthController@singup');
