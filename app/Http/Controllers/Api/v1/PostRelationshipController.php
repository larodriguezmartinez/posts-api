<?php

namespace App\Http\Controllers\Api\v1;

use App\Post;
use Illuminate\Http\Request;
use App\Http\Resources\UserResource;
use App\Http\Resources\CommentsResource;
use App\Http\Controllers\Controller;

class PostRelationshipController extends Controller
{
    public function author(Post $post)
    {

        return new UserResource($post->author);
    }

    public function comments(Post $post)
    {
        return CommentsResource::collection($post->comments);
    }

}
