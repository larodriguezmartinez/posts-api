<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\User::class)->times(15)->create();
        factory(\App\Post::class)->times(20)->create();
        factory(\App\Comment::class)->times(70)->create();
        // $this->call(UsersTableSeeder::class);
    }
}
