<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PostCommentsRelationshipCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        $post = $this->additional['post'];

        return [

            'links' => [
                'self' => route('posts.relationship.comments', ['posts' => $post->id] ),
                'related' => route('posts.comments', ['posts' => $post->id] ),

            ],

            'data' => CommmentIdentifierResource::collection($this->collection),



        ];
    }
}
