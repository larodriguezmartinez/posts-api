<?php

namespace Tests\Feature;

use App\Post;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;

class PostTest extends TestCase
{

    use WithFaker, RefreshDatabase;

    /**
     *
     * @test
     */
    public function store_Post()
    {

        $user = factory('App\User')->create();

        $data = [
            'title' => $this->faker->sentence($numWords = 6, $variableWords = true ),
            'author_id' => $user->id,
            'content' => $this->faker->text($maxChars = 250),
        ];

        $response = $this->json('POST', $this->baseUrl."posts", $data);

        $response->assertStatus(201);

        $this->assertDatabaseHas('posts', $data);

        $post = Post::all()->first();

        $response->assertJson([
            'data' => [
                'id' => $post->id,
                'title' => $post->title
            ]
        ]);


    }

    /**
     *
     * @test
     */
    public function delete_Post()
    {

        $user = factory('App\User')->create();

        $post = factory('App\Post')->create();

        $this->json('DELETE', $this->baseUrl. "posts/{$post->id}")->assertStatus(204);

        $this->assertNull(Post::find($post->id));


    }

    /**
     *
     * @test
     */
    public function update_Post()
    {

        $user = factory('App\User')->create();

        $data = [
            'title' => $this->faker->sentence($numWords = 6, $variableWords = true ),
            'content' => $this->faker->text($maxChars = 250),
        ];

        $post = factory('App\Post')->create();

        $response = $this->json('PUT', $this->baseUrl. "posts/{$post->id}", $data)->assertStatus(200);

        $post = $post->fresh();

        $this->assertEquals($post->title, $data['title']);

        $this->assertEquals($post->content, $data['content']);


    }

        /**
     *
     * @test
     */
    public function show_Post()
    {

        $user = factory('App\User')->create();

        $post = factory('App\Post')->create();

        $response = $this->json('GET', $this->baseUrl. "posts/{$post->id}")->assertStatus(200);


        $response->assertJson([
            'data' => [
                'id' => $post->id,
                'title' => $post->title
            ]
        ]);


    }

}
