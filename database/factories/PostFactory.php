<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Post;
use App\User;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {
    return [
       'title' => $faker->sentence($numWords = 6, $variableWords = true ),
       'author_id' => User::all()->random(),
       'content' => $faker->text($maxChars = 250),
    ];
});
